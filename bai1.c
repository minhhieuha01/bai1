#include<stdio.h>
#include<stdint.h>
#include<string.h>
void ReverseString(char* s)
{
	int i = 0;
	int n = strlen(s)-1;
	while (i < n)
	{
		char temp = s[i];
		s[i] = s[n];
		s[n] = temp;
		i++;
		n--;
	}
}
int main()
{
	char s[] = "itrvietnam-2022";
	ReverseString(s);
	printf("%s", s);
	return 0;
}
